class Vertex extends PVector
{
  private boolean selectFlag = false;
  private PVector mouseVector;
  
  private String input = "none";
  private int outputBusIndex = -1;
  
  private int inputMod;
  private int outputMod;
  
  Synth sourceSynth;
  Synth inputSynth;
  Synth feedbackSynth;
  Synth outputSynth;
  
//  private Bus sourceBus;
//  private Bus feedbackInBus;
//  private Bus feedbackOutBus;
  
  private int sourceBusIndex;
  private int feedbackInBusIndex;
  private int feedbackOutBusIndex;
  
  Allocator busAllocator;
  
  Vertex(float x, float y, Allocator b){
    this.x = x;
    this.y = y;
    this.busAllocator = b;
    inputMod = floor(random(0, 1) * 64);
    outputMod = floor(random(0, 1) * 64); 
//    println("source bus: " + sourceBus.index);
//    println("feedback out bus: " + feedbackOutBus.index);
//    input = (int) random(-1,8);
//    output =(int) random(-1,8);
  }
  
  void display() {
    pushMatrix();
    translate(x, y, 0);
    if(isSelected()){
      shininess(1.0);
    } else {
      shininess(10.0);
    }
    sphere(10);
    popMatrix();
  }

  public boolean isMouseOver(){
    PVector mousePos = new PVector(mouseX, mouseY);
    if(  PVector.dist(this, mousePos) <= 10){
      return true;
    } else {
      return false;
    }
  }
  
  void setSelected(boolean b){
    selectFlag = b;
  }
  
  boolean isSelected(){
     return selectFlag;
  }
  
  void setMouseVector(PVector v){
    mouseVector = v;
  }
  
  PVector getMouseVector(){
     return mouseVector;
  }
  
  void setInput(String i){
    input = i;
  }
  
  String getInput(){
     return input;
  }
  
  void setInputMod(int m){
    if(inputSynth != null){
      inputSynth.set("mod", pow(2,m));
     }
     inputMod = m;
  }
  
  int getInputMod(){
     return inputMod;
  }
  
  void setOutputMod(int m){
   if(outputSynth != null){
      outputSynth.set("mod", pow(2,m));
   }
      outputMod = m;
  }
  
  int getOutputMod(){
     return outputMod;
  }
  
  int getSourceBusIndex(){
//     return sourceBus.index;
     return sourceBusIndex;
  }
  
  
  int getFeedbackInBusIndex(){
//     return feedbackInBus.index;
     return feedbackInBusIndex;
  }
  
  int getFeedbackOutBusIndex(){
//     return feedbackOutBus.index;
     return feedbackOutBusIndex;
  }
  
  void setSourceBusIndex(int i){
     sourceBusIndex = i;
  }
  
  void setFeedbackInBusIndex(int i){
     feedbackInBusIndex = i;
  }
  
  void setFeedbackOutBusIndex(int i){
     feedbackOutBusIndex = i;
  }
  
  int getOutputBusIndex(){
     return outputBusIndex;
  }
  
  void setOutputBusIndex(int i){
     outputBusIndex = i;
  }
  
  void startFeedbackSynth(Group feedbackGroup){
    stopFeedbackSynth();
    
//    feedbackInBus = new Bus();
    setFeedbackInBusIndex(busAllocator.getNextAvailableIndex());
//    println(getFeedbackInBusIndex());
//    feedbackOutBus = new Bus();
    setFeedbackOutBusIndex(busAllocator.getNextAvailableIndex());
    
    feedbackSynth = new Synth("feedback");
    feedbackSynth.set("busIn", getFeedbackInBusIndex());
    feedbackSynth.set("busOut", getFeedbackOutBusIndex());
    feedbackSynth.addToTail(feedbackGroup);
  }
  
  void startSourceAndInputSynths(String syhthDefName, Group sourceGroup, Group inputGroup){
     stopSourceAndInputSynths();
//     sourceBus = new Bus();
     setSourceBusIndex(busAllocator.getNextAvailableIndex());
     
//     println(getSourceBusIndex());
     startSourceSynth(syhthDefName, sourceGroup);
     startInputSynth(inputGroup);
  }
  
  void startSourceSynth(String syhthDefName, Group sourceGroup){
    stopSourceSynth();
    input = syhthDefName;
    sourceSynth = new Synth(syhthDefName);
    sourceSynth.set("busOut", getSourceBusIndex());
    sourceSynth.addToTail(sourceGroup);
  }
  
   void startInputSynth(Group inputGroup){
    stopInputSynth();
    inputSynth = new Synth("input");
    inputSynth.set("busIn", getSourceBusIndex());
    inputSynth.set("busOut", getFeedbackInBusIndex());
    inputSynth.set("mod", inputMod);
    inputSynth.addToTail(inputGroup);
  }
  
  void startOutputSynth(int i, Group outputGroup){
    stopOutputSynth();
    setOutputBusIndex(i);
    outputSynth = new Synth("output");
    outputSynth.set("busIn", getFeedbackOutBusIndex());
    outputSynth.set("busOut", getOutputBusIndex());
    outputSynth.set("mod", outputMod);
    outputSynth.addToTail(outputGroup);
  }
  
  void stopSourceAndInputSynths(){
    stopSourceSynth();
    stopInputSynth();
  }
  
  void stopSourceSynth(){
    if(sourceSynth != null){
      input = "none";
      sourceSynth.free();
//      sourceBus.free();
      busAllocator.freeIndex(getSourceBusIndex());
    }
  }
  
  void stopInputSynth(){
    if(inputSynth != null){
     inputSynth.free();
    }
  }
  
  void stopFeedbackSynth(){
    if(feedbackSynth != null){
      feedbackSynth.free();
//      feedbackInBus.free();
//      feedbackOutBus.free();
      busAllocator.freeIndex(getFeedbackInBusIndex());
      busAllocator.freeIndex(getFeedbackOutBusIndex());
    }
  }
  
  void stopOutputSynth(){
    if(outputSynth != null){
      setOutputBusIndex(-1);
      outputSynth.free();
    }
  }
  
  void stopAllSynths(){
    stopSourceSynth();
    stopInputSynth();
    stopFeedbackSynth();
    stopOutputSynth();
  }
  
}
