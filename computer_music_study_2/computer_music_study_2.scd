(
a = Buffer.read(s, "/home/mwinter/room_ambience.wav"); //buffer of recording of the ambient noise; replace accordingly
)


(
SynthDef("help-Select",{ arg out=0;
	Out.ar([0,1], Clip.ar(PlayBuf.ar(1, a, 1) * pow(Line.ar(0,1, 60 * 3.5, doneAction: 2), 3) * 2000, 0, 1));
}).play;
)