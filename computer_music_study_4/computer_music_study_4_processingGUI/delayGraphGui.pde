import processing.opengl.*;
import supercollider.*;
import oscP5.*;
import netP5.*;

// does removing this input fucking anything else up?
//import java.awt.event.*;

ArrayList<Edge> edges = new ArrayList<Edge>();
ArrayList<Vertex> vertices = new ArrayList<Vertex>();
ArrayList<Vertex> path = new ArrayList<Vertex>();
String data = "";
ArrayList<String> synthDefNames = new ArrayList<String>();
Group feedbackGroup;
Group sourceGroup;
Group inputGroup;
Group edgeGroup;
Group outputGroup;
//int windowX = 1280;
//int windowY = 720;
int windowX = 1050;
int windowY = 1050;
String tool = "edit";
int delayControlMult = 1;

OscP5 queryTreeOsc;

Allocator busAllocator = new Allocator(64);
Allocator bufferAllocator = new Allocator(0);

void setup()  { 

// needed for old mousewheel version 
//  addMouseWheelListener(new MouseWheelListener() { 
//    public void mouseWheelMoved(MouseWheelEvent mwe) { 
//      mouseWheel(mwe.getWheelRotation());
//  }}); 
  
  Server.local.default_group.freeAll();
  
  feedbackGroup = new Group();
  sourceGroup = new Group();
  inputGroup = new Group();
  edgeGroup = new Group();
  outputGroup = new Group();
    
  feedbackGroup.addToTail();
  sourceGroup.addToTail();
  inputGroup.addToTail();
  edgeGroup.addToTail();
  outputGroup.addToTail();
  
  size(windowX, windowY, OPENGL);
  
  queryTreeOsc = new OscP5(this, 57151);
  queryTreeOsc.properties().setSRSP(OscProperties.ON);
  
  synthDefNames.add("none");
 
}   

void exit(){
  Server.local.default_group.freeAll();
}

void oscEvent(OscMessage msg) {
  for(int i = 0; i < msg.typetag().length(); i++){
   String val =  msg.get(i).stringValue();
   synthDefNames.add(val); 
   println(val); 
  }
}

void draw()  {    

  background(0);
  noStroke();

  fill(0, 51, 102); 
  ambientLight(102, 102, 102);
  lightSpecular(204, 204, 204); 
  directionalLight(102, 102, 102, 0, 0, -1); 
  specular(255, 255, 255); 
            
  for (int i = 0; i < vertices.size(); i++) { 
    vertices.get(i).display();
  }  
  
  for (int i = 0; i < edges.size(); i++) { 
    edges.get(i).display();
  }  
  
  updateDataDisplay();
  //text(data, 15, 30); 
  
  //text((synthDefNames), windowX - 115, 30); 
  textSize(20);
  text(data, 15, 200); 
  text(getFormattedSynthDefNames(synthDefNames), windowX - 200, 200); 
  
  if(mouseX < 50) {
    cursor(CROSS);
  } else {
    cursor(HAND);
  }
}     

void mousePressed() {
  
  Vertex mouseOverVertex = getMouseOverVertex();

  if(mouseOverVertex == null && keyCode == SHIFT && keyPressed && tool == "edit"){
    Vertex vertex = new Vertex(mouseX, mouseY, busAllocator);
    vertex.startFeedbackSynth(feedbackGroup);
    vertex.setSelected(true);
    vertices.add(vertex);
   } else if(mouseOverVertex != null && keyCode == SHIFT && keyPressed && tool == "edit"){
     removeVertex(mouseOverVertex);
   } else if(mouseOverVertex != null){
    if(mouseOverVertex.isSelected()){
      mouseOverVertex.setSelected(false);
     } else {
      mouseOverVertex.setSelected(true);
     }
  } else if(tool == "path"){
    for(int i = 0; i < vertices.size(); i++) { 
         vertices.get(i).setSelected(false);
     } 
    path = new ArrayList<Vertex>();
  }
}

void mouseReleased() {
  if(tool == "path" && keyCode != SHIFT){
    for(int i = 0; i < vertices.size(); i++) { 
         vertices.get(i).setSelected(false);
     }  
  }
}

void mouseMoved() {
} 

void mouseDragged(){
  Vertex mouseOverVertex = getMouseOverVertex();
  if(tool == "move"){
    ArrayList<Vertex> selectedVertices = getSelectedVertices();
    for(int i = 0; i < selectedVertices.size(); i++){
       PVector mouseVector = new PVector(mouseX, mouseY);
       PVector lastMouseVector = new PVector(pmouseX, pmouseY);
       selectedVertices.get(i).add(PVector.sub(mouseVector, lastMouseVector));
    }
  } else if(tool == "path" && !keyPressed){
    if(path.size() == 0 && mouseOverVertex != null){
       mouseOverVertex.setSelected(true);
       path.add(mouseOverVertex);
    } else if (mouseOverVertex != null) {
      mouseOverVertex.setSelected(true);
      path.add(mouseOverVertex);
      Vertex outVertex = path.get(path.size()-2);
      Vertex inVertex = path.get(path.size()-1);
      Edge edge = new Edge(outVertex, inVertex, bufferAllocator);
      if(outVertex != inVertex && !edges.contains(edge)){
          edge.startSynth(edgeGroup);
          edges.add(edge);
        }
      }
  } else if(tool == "path" && keyPressed &&  key == CODED && keyCode == SHIFT){
    if(path.size() == 0 && mouseOverVertex != null){
       mouseOverVertex.setSelected(true);
       path.add(mouseOverVertex);
    } else if (mouseOverVertex != null){
      mouseOverVertex.setSelected(true);
      path.add(mouseOverVertex);
      Vertex outVertex = path.get(path.size()-2);
      Vertex inVertex = path.get(path.size()-1);
      Edge edge = new Edge(outVertex, inVertex, bufferAllocator);
      if(outVertex != inVertex && edges.contains(edge)){
        edge = edges.get(edges.indexOf(edge));
        edge.stopSynth();
        edges.remove(edge);
        }
      }
  }
}

void keyPressed(){
  
  Vertex mouseOverVertex = getMouseOverVertex();
  ArrayList<Vertex> selectedVertices = getSelectedVertices();
  
  if (key == 'p'){
    tool = "path";
  } else if (key == 'e'){
    tool = "edit";
  } else if (key == 'm'){
    tool = "move";
  } else if (key == 'g'){
    tool = "global";
  } else if (key == 'c'){
    for(int i = 0; i < selectedVertices.size(); i++) { 
      for(int j = 0; j < selectedVertices.size(); j++) { 
        Edge edge = new Edge(selectedVertices.get(i), selectedVertices.get(j), bufferAllocator);
        if(i != j && !edges.contains(edge)){
          edge.startSynth(edgeGroup);
          edges.add(edge);
        }
      }
    }
   } else if(key == 'd'){
     for(int i = 0; i < vertices.size(); i++) { 
         vertices.get(i).setSelected(false);
     }  
   } else if(key == 'a'){
       for(int i = 0; i < vertices.size(); i++) { 
         vertices.get(i).setSelected(true);
       }  
   } else if(key == '-'){
     delayControlMult = constrain(delayControlMult - 1, 1, 10);
   } else if(key == '='){
     delayControlMult = constrain(delayControlMult + 1, 1, 10);
   } else if(key == 'i'){
     saveFrame("/home/mwinter/delayGraphImages/delayGraph-######.png");
   } else if(keyCode == BACKSPACE){
     for(int i = 0; i < selectedVertices.size(); i++){
       removeVertex(selectedVertices.get(i));
     }
   } else if(mouseOverVertex != null && isInteger(key)){
      int i = Character.getNumericValue(key);
      if(mouseOverVertex.x > mouseX){
        if(i == 0){
          mouseOverVertex.stopSourceAndInputSynths();
        } else {
          println(synthDefNames.get(i));
          mouseOverVertex.startSourceAndInputSynths(synthDefNames.get(i), sourceGroup, inputGroup);
        }
       } else{
        if(i == 0){
          mouseOverVertex.stopOutputSynth();
        } else {
          mouseOverVertex.startOutputSynth(i - 1, outputGroup);
        }
       }
   }
}

void removeVertex(Vertex v){
  int i = 0;
  while(i < edges.size()){
    Edge edge = edges.get(i);
    if(edge.getInVertex() == v || edge.getOutVertex() == v){
      edge.stopSynth();
      edges.remove(edge);
    } else{
      i++;
    }
  }
  v.stopAllSynths();
  vertices.remove(v); 
}

Edge getMouseOverEdge(){
  Edge mouseOverEdge = null;
  for(int i = 0; i < edges.size(); i++) { 
    if(edges.get(i).isMouseOver()){
      mouseOverEdge = edges.get(i);
      break;
    }
  }
  return mouseOverEdge;
}

Vertex getMouseOverVertex(){
  Vertex mouseOverVertex = null;
  for(int i = 0; i < vertices.size(); i++) { 
    if(vertices.get(i).isMouseOver()){
      mouseOverVertex = vertices.get(i);
      break;
    }
  }
  return mouseOverVertex;
}

ArrayList<Vertex> getSelectedVertices(){
  ArrayList<Vertex> selectedVertices = new ArrayList<Vertex>();
  for(int i = 0; i < vertices.size(); i++) { 
    if(vertices.get(i).isSelected()){
      selectedVertices.add(vertices.get(i));
    }
  }
  return selectedVertices;
}

public boolean isInteger( char i ) {  
   try {  
      Integer.parseInt( String.valueOf(i) );  
      return true;  
   } catch(Exception e) {  
      return false;  
   }  
} 

void updateDataDisplay(){
  Vertex mouseOverVertex = getMouseOverVertex();
  Edge mouseOverEdge = getMouseOverEdge();
  if(mouseOverVertex != null){
      String outputString;
      if(mouseOverVertex.getOutputBusIndex() == -1){
        outputString = "none";
      } else {
        outputString = (mouseOverVertex.getOutputBusIndex() + 1) + "";
      }
       data = "input: " + mouseOverVertex.getInput() + "\nouput: " + outputString + 
         "\ninput mod: " + mouseOverVertex.getInputMod() + "\noutput mod: " + mouseOverVertex.getOutputMod();
    } else if(mouseOverEdge != null){
      String mouseOverEdgeDir = "";
      if(mouseOverEdge.getOutVertex().x < mouseOverEdge.getInVertex().x){
        mouseOverEdgeDir = "->";
      } else {
        mouseOverEdgeDir = "<-";
      }
      String data1 = mouseOverEdgeDir + "\ndelay time: " + mouseOverEdge.getDelayTime() + "\nmod: " + mouseOverEdge.getMod();
      String data2 = "";
      
      Edge sharedEdge = getSharedEdge(edges, mouseOverEdge);
      if(sharedEdge != null){
        String sharedEdgeDir = "";
        if(sharedEdge.getOutVertex().x < sharedEdge.getInVertex().x){
          sharedEdgeDir = "->";
        } else {
          sharedEdgeDir = "<-";
        }
        data2 = sharedEdgeDir + "\ndelay time: " + sharedEdge.getDelayTime() + "\nmod: " + sharedEdge.getMod();
      }
      data = data1 + "\n" + data2;
    } else {
      data = "";
    }
}

String getFormattedSynthDefNames(ArrayList<String> s){
 String res = "synth definitions:\n";
 for(int i = 0; i < s.size(); i++){
  res = res +  i + ") " + s.get(i) + "\n";
 }
 return res;
}

Edge getSharedEdge(ArrayList<Edge> e, Edge edge){
  if(edge == null || e == null || e.size() < 1){
    return null;
  }
  
  for(int i = 0; i < e.size(); i++) { 
    if(edge.getOutVertex() == e.get(i).getInVertex() && edge.getInVertex() == e.get(i).getOutVertex()){
      return e.get(i);
    }
  }
  
  return null;
}

void mouseWheel(MouseEvent me) {
  Vertex mouseOverVertex = getMouseOverVertex();
  Edge mouseOverEdge = getMouseOverEdge();
  Edge sharedEdge = getSharedEdge(edges, mouseOverEdge);
  int delta = (int) me.getAmount();
     if(tool == "edit" && mouseOverVertex != null){
      if(mouseOverVertex.x > mouseX){
        mouseOverVertex.setInputMod(constrain(mouseOverVertex.getInputMod() - delta, 1, 64));
      } else{
        mouseOverVertex.setOutputMod(constrain(mouseOverVertex.getOutputMod() - delta, 1, 64));
      }
     } else if(tool == "edit" && mouseOverEdge != null && mouseOverEdge.isMouseOverMod()){
       mouseOverEdge.setMod(constrain(mouseOverEdge.getMod() - delta, 1, 64));
     } else if(tool == "edit" && mouseOverEdge != null && mouseOverEdge.isMouseOverDelayTime()){
       mouseOverEdge.setDelayTime(constrain(mouseOverEdge.getDelayTime() - delta/pow(10.,delayControlMult), 0, mouseOverEdge.getMaxDelayTime()));
     } else if(tool == "edit" && sharedEdge != null && sharedEdge.isMouseOverMod()){
       sharedEdge.setMod(constrain(sharedEdge.getMod() - delta, 1, 64));
     } else if(tool == "edit" && sharedEdge != null && sharedEdge.isMouseOverDelayTime()){
       sharedEdge.setDelayTime(constrain(sharedEdge.getDelayTime() - delta/pow(10.,delayControlMult), 0, mouseOverEdge.getMaxDelayTime()));
     } else if(tool == "global"){
       if(windowX/2 > mouseX){
         for(int i = 0; i < edges.size(); i++){
          edges.get(i).setMod(constrain(edges.get(i).getMod()  - delta, 1, 64));
         }
      } else{
        for(int i = 0; i < edges.size(); i++){
          edges.get(i).setDelayTime(constrain(edges.get(i).getDelayTime() - delta/pow(10.,delayControlMult) , 0, 11.8886));
         }
      }
     }
}


//old mousewheel version
//void mouseWheel(MouseEvent me) {
//  Vertex mouseOverVertex = getMouseOverVertex();
//  Edge mouseOverEdge = getMouseOverEdge();
//  Edge sharedEdge = getSharedEdge(edges, mouseOverEdge);
//  int delta = (int) me.getAmount();
//     if(tool == "edit" && mouseOverVertex != null){
//      if(mouseOverVertex.x > mouseX){
//        mouseOverVertex.setInputMod(constrain(mouseOverVertex.getInputMod() - delta/2, 1, 64));
//      } else{
//        mouseOverVertex.setOutputMod(constrain(mouseOverVertex.getOutputMod() - delta/2, 1, 64));
//      }
//     } else if(tool == "edit" && mouseOverEdge != null && mouseOverEdge.isMouseOverMod()){
//       mouseOverEdge.setMod(constrain(mouseOverEdge.getMod() - delta/2, 1, 64));
//     } else if(tool == "edit" && mouseOverEdge != null && mouseOverEdge.isMouseOverDelayTime()){
//       mouseOverEdge.setDelayTime(constrain(mouseOverEdge.getDelayTime() - delta/1000., 0, mouseOverEdge.getMaxDelayTime()));
//     } else if(tool == "edit" && sharedEdge != null && sharedEdge.isMouseOverMod()){
//       sharedEdge.setMod(constrain(sharedEdge.getMod() - delta/2, 1, 64));
//     } else if(tool == "edit" && sharedEdge != null && sharedEdge.isMouseOverDelayTime()){
//       sharedEdge.setDelayTime(constrain(sharedEdge.getDelayTime() - delta/1000., 0, mouseOverEdge.getMaxDelayTime()));
//     } else if(tool == "global"){
//       if(windowX/2 > mouseX){
//         for(int i = 0; i < edges.size(); i++){
//          edges.get(i).setMod(constrain(edges.get(i).getMod()  - delta/2, 1, 64));
//         }
//      } else{
//        for(int i = 0; i < edges.size(); i++){
//          edges.get(i).setDelayTime(constrain(edges.get(i).getDelayTime() - delta/1000., 0, 11.8886));
//         }
//      }
//     }
//}

//Vertex getNearestVertex(ArrayList<Vertex> v, float x, float y){
//  if(v.size() == 0){
//    return null;
//  }
//  
//  Vertex nearestVertex = v.get(0);
//  float distanceToNearestVertex = dist(nearestVertex.x, nearestVertex.y, x, y);
//  for(int i = 1; i < v.size(); i++) { 
//    Vertex curVertex = v.get(i);
//    float curDistance =  dist(curVertex.x, curVertex.y, x, y);
////    println("i = " + i + ", curD = " + curDistance + ", x = " + x + ", y = " + y);
//    if(curDistance < distanceToNearestVertex){
//      nearestVertex = curVertex;
//      distanceToNearestVertex = curDistance;
//    }
//  }
//  return nearestVertex;
//}
//
//float edgeDist(Edge e, float x, float y){
//  float distance = dist(e.getOutVertex().x, e.getOutVertex().y, x, y) + 
//    dist(x, y, e.getInVertex().x, e.getInVertex().y) -
//      dist(e.getOutVertex().x, e.getOutVertex().y, e.getInVertex().x, e.getInVertex().y);
//  return distance;
//}
//
//Edge getNearestEdge(ArrayList<Edge> e, float x, float y){
//  if(e.size() == 0){
//    return null;
//  }
//  
//  Edge nearestEdge = e.get(0);
//  float distanceToNearestEdge = edgeDist(nearestEdge, x, y);
//  for(int i = 1; i < e.size(); i++) { 
//    Edge curEdge = e.get(i);
//    float curDistance = edgeDist(curEdge, x, y);
////    println("i = " + i + ", curD = " + curDistance + ", x = " + x + ", y = " + y);
//    if(curDistance < distanceToNearestEdge){
//      nearestEdge = curEdge;
//      distanceToNearestEdge = curDistance;
//    }
//  }
//  return nearestEdge;
//}
