(
~oscListeners.do({ arg item, i; ~oscListeners[i].free});
~oscListeners = List.new;

~streams.do({ arg item, i; ~oscListeners[i].stop});
~streams = Dictionary.new;

~oscListeners.add(
	OSCFunc({ |msg, time, addr |
		msg.postln;
		Pbindef(msg[1].asSymbol, msg[2].asSymbol,Pseq(msg[3..]));
		Pbindef('PBind0','\bufnum',e.bufnum);
	}, '/bind' );
);
~oscListeners.add(
	OSCFunc({ |msg, time, addr |
		msg.postln;
		Pbindef(msg[1].asSymbol,'instrument',msg[3].asSymbol);
		Pbindef(msg[1].asSymbol,'\bufnum',e.bufnum);
	}, '/ins' );
);
~oscListeners.add(
	OSCFunc({ |msg, time, addr |
		msg.postln;
		Pbindef(msg[1].asSymbol).playOnce;
	}, '/playPBind' );
);
~oscListeners.add(
	OSCFunc({ |msg, time, addr |
		msg.postln;
		(all {: m.asSymbol, m <- msg[2..] }).postln;
		~streams.put(msg[1], Ppar(all {: Pdef(m.asSymbol), m <- msg[2..] }).play);
	}, '/playPPar' );
);
~oscListeners.add(
	OSCFunc({ |msg, time, addr |
		msg.postln;
		~streams.at(msg[1]).stop;
	}, '/stopPPar' );
);
)

(
e = Buffer.alloc(s, 512, 1);
e.sine1(1.0, true, true, true);

SynthDef(\difference, { |dur = 1, outbus = 0, bufnum = 0, freq = 440, amp = 1, gate = 1|
    var out;
	out = EnvGen.ar(Env.adsr(sustainLevel:1), gate, doneAction: 2) * amp * Osc.ar(e.bufnum, freq, 0, 1, 0);
    Out.ar(outbus, out ! 2);
}).add;
)
