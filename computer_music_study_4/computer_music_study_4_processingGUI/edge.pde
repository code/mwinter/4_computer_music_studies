class Edge
{
  private Vertex inVertex;
  private Vertex outVertex;
  private float velocity;
  private float distance;
  private float delayTime;
  private float maxDelayTime = 11.8886;
  private int mod;
  private Synth edgeSynth;
  private int bufferIndex;
  Allocator bufferAllocator;
  
  Edge(Vertex outVertex, Vertex inVertex, Allocator b){
    this.inVertex = inVertex;
    this.outVertex = outVertex;
    bufferAllocator = b;
    distance = 0;
    mod = floor(random(0, 1) * 64);
    delayTime = random(.01, maxDelayTime);
  }
  
  void display() {
    
    PVector dir = PVector.sub(outVertex, inVertex);
    float r = sqrt(dir.x*dir.x+dir.y*dir.y+dir.z*dir.z);
    float theta = atan2(dir.y,dir.x);
    float phi = acos(dir.z/r);
    float angle = atan2(outVertex.y-inVertex.y, outVertex.x-inVertex.x);
    
    float vDist = PVector.dist(inVertex, outVertex);
    
    distance += abs(delayTime - 1) * 1/60. + 1/60.;
    dir.mult(distance);
    PVector location = PVector.sub(outVertex, dir);
    
    if(distance + abs(delayTime-1) * 1/60. + 1/60. > 1) {
      distance = 0;
    } 
    
    pushMatrix();
    
    shininess(10.0);
    
    translate(location.x, location.y, 0);
    sphere(5);
    translate(-location.x, -location.y, 0);
    
    translate(outVertex.x, outVertex.y, outVertex.z);
    rotateZ(theta);
    rotateY(phi);
    rotateX(-HALF_PI);
    cylinder(1, vDist, 10);
    translate(-outVertex.x, -outVertex.y, -outVertex.z);
    
    popMatrix();
  }
  
  public boolean isMouseOver(){
    if(edgeDist(this, mouseX, mouseY) <= 4){
      return true;
    } else{
      return false;
    }
  }
  
 public boolean isMouseOverMod(){
    if(modDist(this, mouseX, mouseY) <= 4){
      return true;
    } else{
      return false;
    }
  }
  
 public boolean isMouseOverDelayTime(){
    if(delayTimeDist(this, mouseX, mouseY) <= 4){
      return true;
    } else{
      return false;
    }
  }
  
 float edgeDist(Edge e, float x, float y){
    float distance = dist(e.getOutVertex().x, e.getOutVertex().y, x, y) + 
      dist(x, y, e.getInVertex().x, e.getInVertex().y) -
        dist(e.getOutVertex().x, e.getOutVertex().y, e.getInVertex().x, e.getInVertex().y);
    return distance;
  }
  
  float modDist(Edge e, float x, float y){
    float midMidPointX=e.getOutVertex().x+((e.getInVertex().x-e.getOutVertex().x)/4.0);
    float midMidPointY=e.getOutVertex().y+((e.getInVertex().y-e.getOutVertex().y)/4.0);
    float distance = dist(e.getOutVertex().x, e.getOutVertex().y, x, y) + 
      dist(x, y, midMidPointX, midMidPointY) -
        dist(e.getOutVertex().x, e.getOutVertex().y, midMidPointX, midMidPointY);
    return distance;
  }
  
  float delayTimeDist(Edge e, float x, float y){
    float midPointX=e.getOutVertex().x+((e.getInVertex().x-e.getOutVertex().x)/2.0);
    float midPointY=e.getOutVertex().y+((e.getInVertex().y-e.getOutVertex().y)/2.0);
    float midMidPointX=e.getOutVertex().x+((e.getInVertex().x-e.getOutVertex().x)/4.0);
    float midMidPointY=e.getOutVertex().y+((e.getInVertex().y-e.getOutVertex().y)/4.0);
    float distance = dist(midMidPointX, midMidPointY, x, y) + 
      dist(x, y, midPointX, midPointY) -
        dist(midPointX, midPointY, midMidPointX, midMidPointY);
    return distance;
  }
  
  public boolean equals(Object obj){
    if(this.getInVertex() == ((Edge) obj).getInVertex() && this.outVertex == ((Edge) obj).getOutVertex()){
      return true;
    } else {
      return false;
    }
  }
  
  void setInVertex(Vertex inVertex){
    this.inVertex = inVertex;
  }
  
  void setOutVertex(Vertex outVertex){
    this.outVertex = outVertex;
  }
  
  Vertex getInVertex(){
    return inVertex;
  }
  
  Vertex getOutVertex(){
    return outVertex;
  }
  
  void setMod(int m){
   if(edgeSynth != null){
      edgeSynth.set("mod", pow(2,m));
   }
    mod = m;
  }
  
  int getMod(){
     return mod;
  }
  
  void setDelayTime(float d){
   if(edgeSynth != null){
      edgeSynth.set("delayTime", d);
   }
    delayTime = d;
  }
  
  float getDelayTime(){
     return delayTime;
  }
  
  void setMaxDelayTime(float d){
   if(edgeSynth != null){
      edgeSynth.set("maxDelayTime", d);
   }
    maxDelayTime = d;
  }
  
  float getMaxDelayTime(){
     return maxDelayTime;
  }
  
  void startSynth(Group g){
    edgeSynth = new Synth("edge");
    
    allocBuffer();
    edgeSynth.set("buffer", bufferIndex);
    
    edgeSynth.set("busIn", outVertex.getFeedbackOutBusIndex());
    edgeSynth.set("busOut", inVertex.getFeedbackInBusIndex());
    edgeSynth.set("mod", getMod());
    edgeSynth.set("maxDelayTime", getMaxDelayTime());
    edgeSynth.set("delayTime", delayTime);
    edgeSynth.addToTail(g);
  }
  
  void stopSynth(){
    edgeSynth.free();
    freeBuffer();
  }
  
  void allocBuffer(){
    bufferIndex = bufferAllocator.getNextAvailableIndex();
    int frames = 524288;
    int channels = 1;
    OscMessage msg = new OscMessage("/b_alloc");
    msg.add(bufferIndex);
    msg.add(frames);
    msg.add(channels);
    Server.osc.send(msg, Server.local.addr);
  }
  
  void freeBuffer(){
    bufferAllocator.freeIndex(bufferIndex);
    OscMessage msg = new OscMessage("/b_free");
    msg.add(bufferIndex);
    Server.osc.send(msg, Server.local.addr);
  }
  
/**
cylinder taken from http://wiki.processing.org/index.php/Cylinder
@author matt ditton
*/
 
void cylinder(float w, float h, int sides){
  float angle;
  float[] x = new float[sides+1];
  float[] z = new float[sides+1];
 
  //get the x and z position on a circle for all the sides
  for(int i=0; i < x.length; i++){
    angle = TWO_PI / (sides) * i;
    x[i] = sin(angle) * w;
    z[i] = cos(angle) * w;
  }
 
  //draw the top of the cylinder
  beginShape(TRIANGLE_FAN);
 
  vertex(0,   0,    0);
 
  for(int i=0; i < x.length; i++){
    vertex(x[i], 0, z[i]);
  }
 
  endShape();
 
  //draw the center of the cylinder
  beginShape(QUAD_STRIP); 
 
  for(int i=0; i < x.length; i++){
    vertex(x[i], 0, z[i]);
    vertex(x[i], h, z[i]);
  }
 
  endShape();
 
  //draw the bottom of the cylinder
  beginShape(TRIANGLE_FAN); 
 
  vertex(0,   h,    0);
 
  for(int i=0; i < x.length; i++){
    vertex(x[i], h, z[i]);
  }
 
  endShape();
}
}
